Metadata-Version: 2.1
Name: APLpy
Version: 2.2.0
Summary: The Astronomical Plotting Library in Python
Author-email: Thomas Robitaille and Eli Bressert <thomas.robitaille@gmail.com>
License: MIT
Project-URL: Homepage, http://aplpy.github.io
Requires-Python: >=3.10
Description-Content-Type: text/x-rst
License-File: LICENSE.md
Requires-Dist: numpy>=1.22
Requires-Dist: astropy>=5.0
Requires-Dist: matplotlib>=3.5
Requires-Dist: reproject>=0.9
Requires-Dist: pyregion>=2.2
Requires-Dist: pillow>=9.0
Requires-Dist: pyavm>=0.9.6
Requires-Dist: scikit-image>=0.20
Requires-Dist: shapely>=2.0
Provides-Extra: test
Requires-Dist: pytest-astropy; extra == "test"
Requires-Dist: pytest-mpl; extra == "test"
Provides-Extra: docs
Requires-Dist: sphinx-astropy; extra == "docs"

|Azure| |CircleCI| |codecov| |Documentation Status|

About
-----

APLpy (the Astronomical Plotting Library in Python) is a Python module
aimed at producing publication-quality plots of astronomical imaging
data in FITS format.

PyPI: https://pypi.python.org/pypi/APLpy

Website: http://aplpy.github.io

Documentation: http://aplpy.readthedocs.io

APLpy is released under an MIT open-source license.

Installing
----------

You can install APLpy and all its dependencies with::

    pip install aplpy

.. |Azure| image:: https://dev.azure.com/thomasrobitaille/aplpy/_apis/build/status/aplpy.aplpy?repoName=aplpy%2Faplpy&branchName=refs%2Fpull%2F441%2Fmerge
   :target: https://dev.azure.com/thomasrobitaille/aplpy/_build/latest?definitionId=16&repoName=aplpy%2Faplpy&branchName=refs%2Fpull%2F441%2Fmerge
.. |CircleCI| image:: https://circleci.com/gh/aplpy/aplpy/tree/main.svg?style=svg
   :target: https://circleci.com/gh/aplpy/aplpy/tree/main
.. |codecov| image:: https://codecov.io/gh/aplpy/aplpy/branch/main/graph/badge.svg
   :target: https://codecov.io/gh/aplpy/aplpy
.. |Documentation Status| image:: https://img.shields.io/badge/docs-latest-brightgreen.svg?style=flat
   :target: https://aplpy.readthedocs.io/en/latest/
