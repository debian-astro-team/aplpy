Source: aplpy
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all,
               python3-astropy,
               python3-matplotlib,
               python3-pil,
               python3-pil.imagetk,
               python3-pyavm,
               python3-pyregion,
               python3-pytest,
               python3-packaging,
               python3-reproject,
               python3-setuptools,
               python3-setuptools-scm,
               python3-shapely
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/aplpy
Vcs-Git: https://salsa.debian.org/debian-astro-team/aplpy.git
Homepage: https://aplpy.github.io/
Rules-Requires-Root: no

Package: python3-aplpy
Architecture: all
Depends: python3-tk,
         ${misc:Depends},
         ${python3:Depends},
         python3-packaging
Suggests: python3-pyavm,
          python3-pyregion
Description: Astronomical Plotting Library in Python
 APLpy is a Python module aimed at producing publication-quality plots
 of astronomical imaging data in FITS format. The module uses
 Matplotlib, a powerful and interactive plotting package. It is capable
 of creating output files in several graphical formats, including EPS,
 PDF, PS, PNG, and SVG.
