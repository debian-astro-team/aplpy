numpy>=1.22
astropy>=5.0
matplotlib>=3.5
reproject>=0.9
pyregion>=2.2
pillow>=9.0
pyavm>=0.9.6
scikit-image>=0.20
shapely>=2.0

[docs]
sphinx-astropy

[test]
pytest-astropy
pytest-mpl
